#!/usr/bin/env sh

set -ex
. ./env.sh

base=docker.io/library/fedora

mkdir -p build

ctr=$(buildah from $base)

buildah run "$ctr" -- dnf -y --setopt=install_weak_deps=False --setopt=tsflags='nodocs' install \
  buildah \
  podman \
  skopeo \
  --exclude container-selinux \
  --exclude fuse-overlayfs
buildah run "$ctr" -- rm -rf /var/cache /var/log/dnf* /var/log/yum.*
buildah add "$ctr" containers.conf /etc/containers/

buildah config --env _BUILDAH_STARTED_IN_USERNS="" "$ctr"
buildah config --env BUILDAH_ISOLATION=chroot "$ctr"
buildah config --env STORAGE_DRIVER=vfs "$ctr"

buildah config --label "name=buildah" "$ctr"
buildah config --label "commit=$CI_COMMIT_SHA" "$ctr"

buildah commit --rm "$ctr" buildah
