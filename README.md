# CI Buildah Image

## Buildah image

Image containing buildah and skopeo to build and manage container inside a
container, based on latest fedora image. Useful for building container images
on GitLab CI shared runner without docker.

## Build locally

Run as unprivileged user, produces `localhost/buildah`.

```sh
$ ./build.sh
```
